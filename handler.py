ACTIVE_COURSE_LIST = ['QNickel12', 'QSilver23', 'QNickel13', 'QCobalt2', 'QNickel14', 'QSilver24', 'QCobalt3']

import os
import sys
import re
import time

rootDir = "/home/nbgrader/"




jupyterPath = "/home/nbgrader/.jupyter/"

for CourseName in ACTIVE_COURSE_LIST:

    print(CourseName)
    path = rootDir + CourseName + "/"

    # nbgrader_config in jupyter
    with open("nbgrader_config.py") as f:
        text = f.read()
        text = re.sub("COURSE_NAME", CourseName, text)

    with open(jupyterPath+"nbgrader_config.py", "w") as f:
        f.write(text)

    # run the course autograding job
    os.system(path+"autogradingjob")

    time.sleep(5)

