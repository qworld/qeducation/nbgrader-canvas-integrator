import os
import configparser

from canvasapi import Canvas
from canvasapi import file, requester

from nbgrader.api import Gradebook, MissingEntry
from nbgrader.apps import NbGrader, NbGraderAPI
from jupyter_core.paths import jupyter_config_path

import json
from bs4 import BeautifulSoup

import time

import logging

logging.basicConfig(
    filename="logautograde.log",
    level=logging.DEBUG,
    filemode='w',
    format='%(levelname)s: %(message)s')

# Load config
config = configparser.ConfigParser()
config.read('config')
c = config['DEFAULT']

# set up nbgrader
app = NbGrader()

# First load all config paths and load them
paths = jupyter_config_path()
paths.insert(0, os.getcwd())
app.config_file_paths.append(paths)
app.load_config_file()

# Everything local happens in this dir
COURSE_DIRECTORY_ROOT = app.config['CourseDirectory']['root']

# the api will do
nbapi = NbGraderAPI(config=app.config)

# open the gradebook now and close at end
gb = Gradebook('sqlite:///' + COURSE_DIRECTORY_ROOT + '/gradebook.db')

# set up canvas object
canvas = Canvas(c['API_URL'], c['API_KEY'])
req_obj = requester.Requester(c['API_URL'], c['API_KEY'])
course = canvas.get_course(c['CANVAS_COURSE_ID'])

# Update local student list if asked
if c.getboolean('BOOL_UPDATE_STUDENT_LIST'):

    # Download students
    print("Adding new students to the nbgrader database")

    users = course.get_users(enrollment_type="student")

    for u in users:
        # check if user exists in nbgrader db
        try:
            gb.find_student(u.id)
        except MissingEntry:
            # if not, then add the student
            print("Adding student " + u.name)
            gb.add_student(u.id,
                           first_name="",
                           last_name=u.name,
#                           email=u.email,
                           lms_user_id=u.id)

# Onwards to processing submissions for each assignment
assign_data = json.loads(c['ASSIGNMENTS'])
for assign in assign_data:
    ASSIGNMENT_ID_CANVAS = assign[0]
    ASSIGNMENT_ID_NBGRADER = assign[1]
    NOTEBOOK_NAME_NBGRADER = assign[2]

    print("Processing: " + ASSIGNMENT_ID_CANVAS + " " + ASSIGNMENT_ID_NBGRADER)

    print("Downloading submissions")
    assignment = course.get_assignment(ASSIGNMENT_ID_CANVAS)

    submissions = assignment.get_submissions()

    # Download the new submissions

    for sub in submissions:
        # if the student has submitted something and it is an uploaded file
        if sub.workflow_state == 'submitted' \
           and sub.submission_type == 'online_upload':

            # need the student user id for later
            student_id = str(sub.user_id)
            print("Processing submission by " + student_id)

            # download only the first entry
            subfile = sub.attachments[0]

            # this is the dir where nbgrader stores the submissions
            attachment_save_dir = COURSE_DIRECTORY_ROOT \
                + "/submitted/" \
                + student_id \
                + "/" \
                + ASSIGNMENT_ID_NBGRADER

            # make dir incase it is not there
            os.makedirs(attachment_save_dir, exist_ok=True)

            # now write the downloaded file in the dir
            attachment_save_path = attachment_save_dir + "/" \
                + NOTEBOOK_NAME_NBGRADER + ".ipynb"

            subfile.download(attachment_save_path)


            # TODO the logic here is quite convoluted
            # Simplify, while ensuring that no errors are thrown
            
            # autograde and generate feedback
            print("Autograding")
            grading_failure = False
            autograde_out = nbapi.autograde(ASSIGNMENT_ID_NBGRADER, student_id, force=True)
            # if grading fails then student gets no score.
            if autograde_out['success'] is False:
                print("Autograding failed")
                logging.debug(autograde_out)
                grading_failure = True
            else:
                print("Generating feedback")
                logging.debug("Uploading feedback for " + student_id)
                feedback_out = nbapi.generate_feedback(ASSIGNMENT_ID_NBGRADER, student_id, force=True)
                if feedback_out['success'] is False:
                    print("feedback process failed")
                    grading_failure = True
                else:
                    # first extract grading information from feedback file
                    # in rare cases there is no feedback file
                    # perhaps student submitted an invalid notebook,
                    # then the  feedback process doesn't complete
                    try:
                        feedback_file_path = COURSE_DIRECTORY_ROOT + '/feedback/' + student_id \
                            + '/' + ASSIGNMENT_ID_NBGRADER + '/' + NOTEBOOK_NAME_NBGRADER + '.html'
                        feedback_file_object = open(feedback_file_path, "r")
                        soup = BeautifulSoup(feedback_file_object.read(), 'html.parser')
                        feedbackhtml = soup.findAll('div', {'class': 'panel-heading'})
                        feedback_text = feedbackhtml[0].get_text()
                    except FileNotFoundError:
                        print("file not found for " + student_id)
                        logging.warning("file not found for " + student_id)
                        grading_failure = True

            if grading_failure is True:
                score = 0.0
                feedback_text = (c['FEEDBACK_ERROR_TEXT'])
            else:
                # In some above case, or otherwise, sometimes the student
                # has no score in the nbgrader db.
                try:
                    submission = gb.find_submission(ASSIGNMENT_ID_NBGRADER, student_id)
                    score = submission.score
                except MissingEntry:
                    score = 0.0
                    feedback_text = (c['FEEDBACK_ERROR_TEXT'])

            # now we can upload
            print('Uploading grade')
            sub.edit(submission={'posted_grade': score}, comment={'text_comment': feedback_text})

            time.sleep(1)

# Don't forget to close the database
gb.close()
