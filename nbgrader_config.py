c = get_config()

c.CourseDirectory.course_id = "COURSE_NAME"

# Root directory of course. 
# Must be a subdirectory of where you run Jupyter notebook.
c.CourseDirectory.root = "/home/nbgrader/COURSE_NAME/Course"

# Exchange server. Create it anywhere on your machine. 
c.Exchange.root = "/home/nbgrader/COURSE_NAME/NbgraderExchange"
