#!/bin/python3
import os
import sys
import shutil
import re

rootDir = "/home/nbgrader/"
# jupyterPath = rootDir + "jupyter/"

jupyterPath = "/home/nbgrader/.jupyter/"

# print("Enter the name of your course, with no spaces (eg. QSilver2)")
# CourseName = "QSilver15"

CourseTypeInput = input(
    '''What type of course is this?
1. QSilver
2. QNickel
3. QCobalt
Enter number: ''')

if CourseTypeInput == '1':
    CourseTye = 1
    CourseTypeName = 'QSilver'
elif CourseTypeInput == '2':
    CourseTye = 2
    CourseTypeName = 'QNickel'
elif CourseTypeInput == '3':
    CourseTye = 2
    CourseTypeName = 'QCobalt'
else:
    print('Input not valid')
    exit()

CourseNumberInput = input(
    '''What is the number of the course?
For instance, if you are setting up QSilver25, enter 25.
Enter number: ''')

if not CourseNumberInput.isdigit():
    print('Input is not valid')
    exit()

CourseName = CourseTypeName + CourseNumberInput


path = rootDir + CourseName + "/"

if os.path.isdir(path):
    print("The folder", path, "already exists. Exiting")
    sys.exit()


# create the course root directory
os.mkdir(path)

# create other directories
os.mkdir(path + "NbgraderExchange")
os.mkdir(path + "Course")

# write necessary files

# assignment files
shutil.copytree(CourseTypeName+'/source', path+"Course/source")


# autogradingjob
with open("autogradingjob") as f:
    text = f.read()
    text = re.sub("COURSE_NAME", CourseName, text)

with open(path+"autogradingjob", "w") as f:
    f.write(text)

os.system("chmod +x "+path+"autogradingjob")

# nbgrader_config internal
shutil.copyfile("nbgrader_config_internal.py", path+"Course/nbgrader_config.py")
# grader and config
shutil.copyfile("grader.py", path+"Course/grader.py")
shutil.copyfile(CourseTypeName+"/config", path+"Course/config")

# nbgrader_config in jupyter
with open("nbgrader_config.py") as f:
    text = f.read()
    text = re.sub("COURSE_NAME", CourseName, text)

with open(jupyterPath+"nbgrader_config.py", "w") as f:
    f.write(text)

# copy assignment release code
shutil.copyfile(CourseTypeName+"/ReleaseAssignments", path+"Course/ReleaseAssignments")
os.system("chmod +x "+path+"Course/ReleaseAssignments")


# release assignments
os.system("cd "+path+"Course; ./ReleaseAssignments")
