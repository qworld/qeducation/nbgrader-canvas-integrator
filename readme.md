# Nbgrader-Canvas Integrator
These set of scripts can be used to integrate [Nbgrader](https://nbgrader.readthedocs.io/en/stable/) and the [Canvas LMS](https://github.com/instructure/canvas-lms). Students can submit Nbgrader coding assignments on Canvas, and their submissions will be graded using Nbgrader and the results will be uploaded to Canvas. A simple cronjob can be used to do this periodically.

# Usage
Usage is quite simple if you are familiar with Nbgrader. First, we setup Nbgrader and Canvas. This 

* Set up Nbgrader on your computer. 
* Create the assignments within Nbgrader. Once done, generate them all.
* For each released assignment, create a corresponding assignment on Canvas, with the released assignment file attached. Students should now be able to submit their solutions on Canvas.


# Setting up this integrator
To set up our intergrator on the server.

Since Ubuntu LTS provides an old version of Python, compile the latest version of Python.
```bash
sudo apt-get install libbz2-dev
sudo apt-get install pkg-config
cd /opt
sudo wget https://www.python.org/ftp/python/3.12.3/Python-3.12.3.tgz
sudo tar xzvf Python-3.12.3.tgz 
cd Python-3.12.3/
sudo ./configure --enable-optimizations
sudo make
sudo make install
sudo ln /opt/Python-3.12.3/python /usr/bin/python3.12
python3.12 --version
```

Now, set up the python virtual environment. Here the requirements file contains requirements for all courses.
```bash
cd ~
python3.12 -m venv environments/envnbgrader
source environments/envnbgrader/bin/activate
python -m pip install -r requirements.txt
python -m pip install nbgrader canvasapi
```

To prevent students from install or uninstalling packages, simply remove pip from the virtual environment. We could also just uninstall it, but the following are reversible steps in case, we want to install something new.
```bash
cd environments/envnbgrader
cd bin/
mv pip backup_pip
mv pip3 backup_pip3
mv pip3.12 backup_pip3.12
cd ../lib/python3.12/site-packages/
mv pip backup_pip
mv pip-24.0.dist-info backup_pip-24.0.dist-info
```

Now set up a cron job.
```bash
crontab -e
```

Add the following line to the file and save. This will run the autograder every 15 minutes.
```
*/15 * * * * cd /home/nbgrader/nbgrader-canvas-integrator/; /home/nbgrader/environments/envnbgrader/bin/python handler.py
```

### Workshop setup
For each workshop, make a new copy of QExample folder. Inside, edit the `config` and `ReleaseAssignments` files appropriately. Paste the source files for the workshop assignments in the source folder.


## Setting up a new workshop

We will assume that you are setting up the QSilver16 course, for clarity. Substitute the actual course code in the following instructions.

1. On Canvas, make a copy of the 'QSilver Template' Course.

2. Go to the 'People' page on Canvas Course site and add the user 'canvasautograder' to the course as a Teacher.

Now you should ssh into the server.

3. First run `SetupServer.py`, and follow the instructions.

```bash
# cd ~/nbgrader-canvas-integrator/
# ./SetupServer.py 
```

All the files and folders are now in place, but require some editing.

4. Edit the config file

```bash
# cd ~/QSilver16/Course/
# nano config 
```

Enter the course id, and the assignment ids. If you open an assignment in a new tab in your browser, it will have an address like `https://study.qworld.net/courses/XXX/assignments/YYYY`. Here `XXX` is the course id and `YYYY` is the assignment id.

5. Now, check if everything is working. First, on Canvas, Publish the course if it is not already. Go one directory higher (`~/QSilver16`), and run the autograder once.

```
# cd ..
# ./autogradingjob 
```

If this runs without errors and students are added, and assignments processed, all is good. Unpublish the course, if it was previously unpublished or its a new course.

6. Go back the nbgrader-canvas-intergrator directory and edit `handler.py`.

```
# cd ~/nbgrader-canvas-integrator/
# nano handler.py
```

Append `QSilver16` to the `ACTIVE_COURSE_LIST` variable.

You are good to go.
